<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\PesananForm;
use yii\httpclient\Client;
use yii\base\Exception;

class MyController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {

        return $this->render('index');
    }

    public function actionPesanan()
    {
        $model = PesananForm::find()->all();

        return $this->render('pesanan', ['model' => $model]);
    }

    public function actionProducts()
    {
        $request = Yii::$app->request;

        if ($request->isAjax) {
            $httpClient = new Client();
            $response = $httpClient->createRequest()
                ->setMethod('GET')
                ->setUrl('https://dummyjson.com/products')
                ->send();

            try {
                if ($response->isOk) {

                    $data = $response->data;

                    $res = [
                        'status' => $response->statusCode,
                        'message' => 'Data found',
                        'data' => $data
                    ];

                    return json_encode($res);
                } else {

                    throw new Exception('API request failed');
                }
            } catch (Exception $e) {

                $res = [
                    'status' => $response->statusCode,
                    'message' => $e->getMessage()
                ];

                return json_encode($res);
            }
        }
    }

    public function actionProductdetail()
    {
        $request = Yii::$app->request;

        if ($request->isAjax) {

            $id = $request->post('id');

            try {
                if (empty($id)) {
                    throw new Exception('Bad Request');
                } else {
                    $httpClient = new Client();
                    $response = $httpClient->createRequest()
                        ->setMethod('GET')
                        ->setUrl('https://dummyjson.com/products/' . $id)
                        ->send();
                }

                if ($response->isOk) {

                    $data = $response->data;

                    $res = [
                        'status' => $response->statusCode,
                        'message' => 'Data found',
                        'data' => $data
                    ];

                    return json_encode($res);
                } else {

                    throw new Exception('API request failed');
                }
            } catch (Exception $e) {

                $res = [
                    'status' => isset($response->statusCode) ? $response->statusCode : 400,
                    'message' => $e->getMessage()
                ];

                return json_encode($res);
            }
        }
    }
}
