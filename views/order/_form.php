<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\models\Order $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'no_pesanan')->textInput(['maxlength' => true])->label('No Pesanan') ?>

    <?= $form->field($model, 'tanggal')->input('datetime-local', ['format' => 'Y-m-d\TH:i:s'])->label('Tanggal') ?>

    <?= $form->field($model, 'nm_supplier')->textInput(['maxlength' => true])->label('Nama Supplier') ?>

    <?= $form->field($model, 'nm_produk')->textInput(['maxlength' => true])->label('Nama Produk') ?>

    <?= $form->field($model, 'total')->textInput(['type' => 'number', 'step' => '0.001'])->label('Total') ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>