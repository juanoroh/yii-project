<?php

use yii\widgets\Pjax;

/** @var yii\web\View $this */

$this->title = 'My Yii Application';

$script = <<< JS
    $('#show-products').on('click', function(){

        $.ajax({
            type: "GET",
            url: "my/products",
            dataType: "json",
            success: function (res) {
                if(res.status == 200){
                    let html = '';
    
                    html += `<table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Image</th>
                                    <th>Title</th>
                                    <th>Category</th>
                                    <th>Brand</th>
                                    <th>Stock</th>
                                    <th>Price</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody id="tbody">`;
                    for (let i = 0; i < res.data.products.length; i++) {
                        const element = res.data.products[i];
                        
                        html += `<tr>
                                    <td><img src="` + element.thumbnail +`" width="150" height="150"></td>
                                    <td>` + element.title +`</td>
                                    <td>` + element.category +`</td>
                                    <td>` + element.brand +`</td>
                                    <td class="text-end">` + element.stock +`</td>
                                    <td class="text-end">$` + element.price +`</td>
                                    <td class="text-center">
                                        <button type="button" class="btn btn-primary btn-view" data-bs-toggle="modal" data-bs-target="#modal-view" data-id="`+ element.id +`">
                                            View
                                        </button>
                                    </td>
                                </tr>`;
                    }
                    html += `</tbody>
                            </table>`;
    
                    $('#product-list').html(html);

                    view();
                }
            }
        });
    })

    function view(){
        $('.btn-view').on('click', function(){
            const id = $(this).data('id');

            $('#product-detail').html('');

            $.ajax({
                type: "POST",
                url: "my/productdetail",
                data: {
                    id: id
                },
                dataType: "json",
                success: function (res) {
                    console.log(res.data);
                    if(res.status == 200){

                        // total number of stars
                        const starTotal = 5;

                        const starPercentage = (res.data.rating / starTotal) * 100;
                        const starPercentageRounded = (Math.round(starPercentage / 10) * 10) + `%`;
                        
                        let html = `<div class="row">
                                        <div class="d-flex justify-content-between">
                                            <h3 class="text-decoration-underline">` + res.data.title + `</h3>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                        </div>
                                        <div class="col">
                                            <div class="m-auto p-2 text-center">
                                                <img src="` + res.data.thumbnail + `" alt="" widht="150" height="200" >
                                            </div>
                                            <div class="d-flex justify-content-between py-2">
                                            <a href="` + res.data.images[0] + `">

                                                <img src="` + res.data.images[0] + `" alt="" height="45">
                                                </a>

                                                <img src="` + res.data.images[1] + `" alt="" height="45">

                                                <img src="` + res.data.images[2] + `" alt="" height="45">

                                                <img src="` + res.data.images[3] + `" alt="" height="45">

                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="d-flex justify-content-between">
                                                <h6>Price: $` + res.data.price + `</h6>
                                                <div class="m-auto pb-1">
                                                    <div class="stars-outer">
                                                        <div class="stars-inner" style="width:`+ starPercentageRounded +`;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col fs-6">
                                                    <div class="pb-2">Category: ` + res.data.category + `</div>
                                                    <div>Stock: ` + res.data.stock + `</div>
                                                </div>
                                                <div class="col fs-6">
                                                    <div>Brand: ` + res.data.brand + `</div>
                                                </div>
                                            </div>
                                            <div class="pt-2">Description:
                                                <p>
                                                ` + res.data.description + `
                                                </p>
                                            </div>
                                        </div>
                                    </div>`
                        
                        $('#product-detail').html(html);
                    }
                }
            });
        })
    }

    
    
JS;
$this->registerJs($script);

?>
<div class="site-index">

    <div class="body-content">
        <div class="card">

            <div class="card-head">
                <h2 class="p-2 m-3 text-decoration-underline">
                    Product List
                </h2>

                <div class="d-flex flex-row-reverse">
                    <button id="show-products" class="btn btn-primary mx-3">
                        Show Products
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div id="product-list"></div>
            </div>

        </div>

    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="modal-view" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div id="product-detail" class="modal-body m-2"></div>
        </div>
    </div>
</div>