<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "t_pesanan".
 *
 * @property int $id
 * @property string $no_pesanan
 * @property string|null $tanggal
 * @property string|null $nm_supplier
 * @property string|null $nm_produk
 * @property float|null $total
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_pesanan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['no_pesanan', 'nm_supplier', 'nm_produk', 'tanggal', 'total'], 'required'],
            [['no_pesanan'], 'unique'],
            [['no_pesanan'], 'string', 'max' => 20],
            [['tanggal'], 'safe'],
            [['nm_supplier', 'nm_produk'], 'string', 'max' => 50],
            [['total'], 'number', 'min' => 0.001],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'no_pesanan' => 'No Pesanan',
            'tanggal' => 'Tanggal',
            'nm_supplier' => 'Nm Supplier',
            'nm_produk' => 'Nm Produk',
            'total' => 'Total',
        ];
    }
}
